package blockchain

//https://bitcoin.org/en/developer-reference#raw-transaction-format
type Transaction struct {
	version uint32
	txIn    []TxIn
	TxOut   []TxOut
	time    uint32
}

type Script []byte
type TxID [32]byte

//Coinbase TxIn after block 227,836 prepends block height to script
type TxIn struct {
	previousOut Outpoint
	script      Script
	sequence    uint32
}

type Outpoint struct {
	hash  TxID
	index uint32
}

type TxOut struct {
	value  int64
	script []byte
}
