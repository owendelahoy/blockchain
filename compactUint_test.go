package blockchain

import (
	"bytes"
	"fmt"
	"testing"
)

func testCompactUintNByteWrite(t *testing.T, value uint64, expectedN int) {
	compactUint := CompactUint(value)
	buffer := bytes.NewBuffer(make([]byte, 0, 9))
	n, err := compactUint.Write(buffer)
	if err != nil {
		fmt.Printf("CompactUint.Write returned error '%s'\n", err.Error())
		t.Fail()
	}
	if n != expectedN {
		fmt.Printf("CompactUint.Write returned %v expected %v\n", n, expectedN)
		t.Fail()
	}
	if buffer.Len() != expectedN {
		fmt.Printf("After CompactUint.Write buffer has %v byte(s) but expected %v byte(s)\n", buffer.Len(), expectedN)
		t.Fail()
	}
}

func TestCompactUint1ByteWrite(t *testing.T) {
	//1 byte int, encoded in 1 bytes
	testCompactUintNByteWrite(t, 0x10, 1)
}

func TestCompactUint3ByteWrite(t *testing.T) {
	//2 byte int, encoded in 3 bytes
	testCompactUintNByteWrite(t, 0x1020, 3)
}

func TestCompactUint5ByteWrite(t *testing.T) {
	//4 byte int, encoded in 5 bytes
	testCompactUintNByteWrite(t, 0x10203040, 5)
}

func TestCompactUint9ByteWrite(t *testing.T) {
	//8 byte int, encoded in 9 bytes
	testCompactUintNByteWrite(t, 0x1020304050607080, 9)
}

func testCompactUintNByteRead(t *testing.T, buffer []byte, expected CompactUint) {
	reader := bytes.NewReader(buffer)
	var compactUint CompactUint
	compactUint.Read(reader)
	if compactUint != expected {
		fmt.Printf("compactUint.Read returned 0x%x expected 0x%x", compactUint, expected)
		t.Fail()
	}
}

func TestCompactUint1ByteRead(t *testing.T) {
	testCompactUintNByteRead(t,
		[]byte{0x10},
		0x10)
}

func TestCompactUint3ByteRead(t *testing.T) {
	testCompactUintNByteRead(t,
		[]byte{0xFD, 0x20, 0x10},
		0x1020)
}

func TestCompactUint5ByteRead(t *testing.T) {
	testCompactUintNByteRead(t,
		[]byte{0xFE, 0x40, 0x30, 0x20, 0x10},
		0x10203040)
}

func TestCompactUint9ByteRead(t *testing.T) {
	testCompactUintNByteRead(t,
		[]byte{0xFF, 0x80, 0x70, 0x60, 0x50, 0x40, 0x30, 0x20, 0x10},
		0x1020304050607080)
}

func testCompactUintWriteRead(t *testing.T, compactUint CompactUint) {
	buffer := bytes.NewBuffer(make([]byte, 0, 9))
	compactUint.Write(buffer)
	var compactUint2 CompactUint
	compactUint2.Read(buffer)
	if compactUint != compactUint2 {
		fmt.Printf("compactUint.Write 0x%x but compactUint.Read 0x%x", compactUint, compactUint2)
		t.Fail()
	}
}

func TestCompactUintWriteRead1byte(t *testing.T) {
	testCompactUintWriteRead(t, 0x1B)
}

func TestCompactUintWriteRead3bytes(t *testing.T) {
	testCompactUintWriteRead(t, 0x1A2B)
}

func TestCompactUintWriteRead5bytes(t *testing.T) {
	testCompactUintWriteRead(t, 0x1A2B3C4D)
}

func TestCompactUintWriteRead9bytes(t *testing.T) {
	testCompactUintWriteRead(t, 0x1A2B3C4D5E6F7A8B)
}

type mockReader struct{}
type mockError struct{}

const mockReaderN = 123

var mockReaderErr mockError

func (mockReader) Read([]byte) (int, error) {
	return mockReaderN, mockReaderErr
}

func (mockError) Error() string {
	return "this is a mock error from mockReader"
}

func TestCompactUintReadError(t *testing.T) {
	reader := mockReader{}
	var compactUint CompactUint
	n, err := compactUint.Read(reader)
	if err != mockReaderErr {
		fmt.Printf("compactUint.Read did not forward error from io.Reader\n")
		t.Fail()
	}
	if n != mockReaderN {
		fmt.Printf("compactUint.Read did not forward n from io.Reader\n")
		t.Fail()
	}
}
