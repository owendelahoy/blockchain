package blockchain

import (
	"crypto/sha256"
	"encoding/binary"
)

const blockHeaderSize = 80

type blockHeaderBytes [blockHeaderSize]byte

//BlockHeader contains the data from a block used in hashing
type BlockHeader struct {
	version uint32
	/*should version be an int32? Surely we can't have negative versions?
	https://bitcoin.org/en/developer-reference#block-headers*/
	previousHash [32]byte
	merkleHash   [32]byte
	time         uint32
	nBits        uint32
	nonce        uint32
}

//GenesisBlockHeader the header of the first block
var GenesisBlockHeader = BlockHeader{
	version: 1,
	merkleHash: [32]byte{
		0x3B, 0xA3, 0xED, 0xFD, 0x7A, 0x7B, 0x12, 0xB2,
		0x7A, 0xC7, 0x2C, 0x3E, 0x67, 0x76, 0x8F, 0x61,
		0x7F, 0xC8, 0x1B, 0xC3, 0x88, 0x8A, 0x51, 0x32,
		0x3A, 0x9F, 0xB8, 0xAA, 0x4B, 0x1E, 0x5E, 0x4A,
	},
	time:  1231006505,
	nBits: 0x1d00ffff,
	nonce: 2083236893,
}

func (header BlockHeader) toBytes() blockHeaderBytes {
	buffer := new([blockHeaderSize]byte)
	binary.LittleEndian.PutUint32(buffer[0:4], header.version)
	copy(buffer[4:36], header.previousHash[:])
	copy(buffer[36:68], header.merkleHash[:])
	binary.LittleEndian.PutUint32(buffer[68:72], header.time)
	binary.LittleEndian.PutUint32(buffer[72:76], header.nBits)
	binary.LittleEndian.PutUint32(buffer[76:80], header.nonce)
	return *buffer
}

func (block blockHeaderBytes) hash() [sha256.Size]byte {
	hash := sha256.Sum256(block[:])
	return sha256.Sum256(hash[:])
}

//Hash calculates the sha256(sha256()) of the block header
func (header BlockHeader) Hash() [sha256.Size]byte {
	return header.toBytes().hash()
}

func (block *blockHeaderBytes) nonce(nonce uint32) {
	binary.LittleEndian.PutUint32(block[76:80], nonce)
}
