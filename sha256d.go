package blockchain

import (
	"crypto/sha256"
	"fmt"
)

type Hash [sha256.Size]byte

func (hash Hash) String() string {
	return fmt.Sprintf("0x%x", [sha256.Size]byte(hash))
}

const HashSize = sha256.Size

func calculateHash(data []byte) Hash {
	hash := sha256.Sum256(data)
	return sha256.Sum256(hash[:])
}
