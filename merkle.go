package blockchain

import "fmt"

type MerkleTree struct {
	root       *MerkleNode
	txidToLeaf map[TxID]*MerkleNode
	height     uint
}

type MerkleBlock []Hash

type MerkleNode struct {
	hash  Hash
	uncle *MerkleNode
}

func (node MerkleNode) String() string {
	return fmt.Sprintf("%x:%x", node.hash[0:4], node.uncle)
}

func (tree MerkleTree) MerkleBlock(txid TxID) MerkleBlock {
	hashes := make([]Hash, 0, tree.height)
	node := tree.txidToLeaf[txid]
	for node != nil {
		if node.uncle != nil {
			hashes = append(hashes, node.hash)
		}
		node = node.uncle
	}
	return hashes
}

func (block MerkleBlock) testHash(hash Hash) Hash {
	node1 := MerkleNode{hash, nil}
	for _, nextHash := range block {
		node2 := MerkleNode{nextHash, nil}
		node1 = merge(node1, node2)
	}
	return node1.hash
}

func merkleNode(data []byte) MerkleNode {
	return MerkleNode{calculateHash(data), nil}
}

//Merges two nodes to create a parent
func merge(a MerkleNode, b MerkleNode) MerkleNode {
	buffer := make([]byte, HashSize*2)
	copy(buffer[0:HashSize], a.hash[:])
	copy(buffer[HashSize:HashSize*2], b.hash[:])
	newNode := merkleNode(buffer)

	return newNode
}

func mergeReduce(data []MerkleNode) []MerkleNode {
	mergeNodes := make([]MerkleNode, 0, (len(data)+1)/2)
	i := 0
	for ; i < len(data)-1; i += 2 {
		mergeNodes = append(mergeNodes, merge(data[i], data[i+1]))
	}
	//If there is a single item remaining, merge it with itself
	if i < len(data) {
		mergeNodes = append(mergeNodes, merge(data[i], data[i]))
	}

	length := len(data)
	if len(mergeNodes)%2 == 1 {
		length--
		uncleIndex := length >> 1
		data[length].uncle = &mergeNodes[uncleIndex]
		length--
		data[length].uncle = &mergeNodes[uncleIndex]
	}
	for i := 0; i < length; i++ {
		uncleIndex := (i >> 1) ^ 1
		data[i].uncle = &mergeNodes[uncleIndex]
	}

	return mergeNodes
}

func merkleRoot(leaves []MerkleNode) (uint, MerkleNode) {
	if len(leaves) == 0 {
		return 0, merkleNode([]byte{})
	}

	nodes := leaves
	var height uint
	for len(nodes) > 1 {
		newNodes := mergeReduce(nodes)
		height++
		nodes = newNodes
	}
	return height, nodes[0]
}

func newMerkleTree(txids []TxID) MerkleTree {
	txidToLeaf := make(map[TxID]*MerkleNode, len(txids))
	leaves := make([]MerkleNode, 0, len(txids))
	for _, txid := range txids {
		leaf := MerkleNode{Hash(txid), nil}
		leaves = append(leaves, leaf)
	}
	length := len(leaves)
	//If we have an odd length, then it's sibling is itself
	if length%2 == 1 {
		length--
		txidToLeaf[txids[length]] = &leaves[length]
	}
	for i := 0; i < length; i++ {
		txidToLeaf[txids[i]] = &leaves[i^1]
	}
	height, rootNode := merkleRoot(leaves)
	return MerkleTree{&rootNode, txidToLeaf, height}
}
