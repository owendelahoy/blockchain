package blockchain

import (
	"encoding/binary"
	"io"
)

//CompactUint implements https://bitcoin.org/en/developer-reference#compactsize-unsigned-integers
type CompactUint uint64

func (compactUint CompactUint) Write(writer io.Writer) (int, error) {
	var bytes []byte
	switch {
	case compactUint < 0xFD: //<=252 <253
		bytes = make([]byte, 1)
		bytes[0] = byte(compactUint)
	case compactUint <= 0xffff:
		bytes = make([]byte, 3)
		bytes[0] = 0xFD
		binary.LittleEndian.PutUint16(bytes[1:3], uint16(compactUint))
	case compactUint <= 0xffffffff:
		bytes = make([]byte, 5)
		bytes[0] = 0xFE
		binary.LittleEndian.PutUint32(bytes[1:5], uint32(compactUint))
	default:
		bytes = make([]byte, 9)
		bytes[0] = 0xFF
		binary.LittleEndian.PutUint64(bytes[1:9], uint64(compactUint))
	}
	return writer.Write(bytes)
}

func (compactUint *CompactUint) Read(reader io.Reader) (n int, err error) {
	buffer := make([]byte, 1, 1)
	n, err = reader.Read(buffer)
	if err != nil {
		return n, err
	}

	var n2 int
	size := buffer[0]
	switch {
	case size < 0xFD:
		*compactUint = CompactUint(size)
	case size == 0xFD:
		buffer = make([]byte, 2)
		n2, err = reader.Read(buffer)
		if err == nil {
			*compactUint = CompactUint(binary.LittleEndian.Uint16(buffer))
		}
	case size == 0xFE:
		buffer = make([]byte, 4)
		n2, err = reader.Read(buffer)
		if err == nil {
			*compactUint = CompactUint(binary.LittleEndian.Uint32(buffer))
		}
	default:
		buffer = make([]byte, 8)
		n2, err = reader.Read(buffer)
		if err == nil {
			*compactUint = CompactUint(binary.LittleEndian.Uint64(buffer))
		}
	}
	return n + n2, err
}
