package blockchain

import (
	"fmt"
	"testing"
)

func TestMerkleNodeZeroMerge(t *testing.T) {
	zero := MerkleNode{}
	merge := merge(zero, zero)
	expected := [HashSize]byte{
		0xe2, 0xf6, 0x1c, 0x3f, 0x71, 0xd1, 0xde, 0xfd,
		0x3f, 0xa9, 0x99, 0xdf, 0xa3, 0x69, 0x53, 0x75,
		0x5c, 0x69, 0x06, 0x89, 0x79, 0x99, 0x62, 0xb4,
		0x8b, 0xeb, 0xd8, 0x36, 0x97, 0x4e, 0x8c, 0xf9,
	}
	if merge.hash != expected {
		fmt.Printf("merge: %x\n", merge.hash)
		fmt.Printf("expected: %x\n", expected)
		t.Fail()
	}
}

func stringToTxID(str string) TxID {
	data := []byte(str)
	return TxID(calculateHash(data))
}

func stringsToTxIDs(strings []string) []TxID {
	txids := make([]TxID, 0, len(strings))
	for _, str := range strings {
		txids = append(txids, stringToTxID(str))
	}
	return txids
}

func TestMerkleTreeEmpty(t *testing.T) {
	tree := newMerkleTree([]TxID{})
	expected := [HashSize]byte{
		0x5d, 0xf6, 0xe0, 0xe2, 0x76, 0x13, 0x59, 0xd3,
		0x0a, 0x82, 0x75, 0x05, 0x8e, 0x29, 0x9f, 0xcc,
		0x03, 0x81, 0x53, 0x45, 0x45, 0xf5, 0x5c, 0xf4,
		0x3e, 0x41, 0x98, 0x3f, 0x5d, 0x4c, 0x94, 0x56,
	}
	if tree.root.hash != Hash(expected) {
		fmt.Printf("tree: %x\n", tree.root.hash)
		fmt.Printf("expected: %x\n", expected)
		t.Fail()
	}
}

func TestMerkleBlock(t *testing.T) {
	strings := []string{"asd", "fgh", "jkl", "qwe", "rty", "uio"}
	hashes := stringsToTxIDs(strings)
	tree := newMerkleTree([]TxID(hashes))
	merkleblock := tree.MerkleBlock(hashes[0])
	hash := merkleblock.testHash(Hash(hashes[0]))
	if tree.root.hash != hash {
		fmt.Printf("tree root: %x\n", tree.root.hash)
		fmt.Printf("hash test: %x\n", hash)
		t.Fail()
	}
}

func TestMerkleNodeMerge(t *testing.T) {
	a := merkleNode([]byte("asd"))
	b := merkleNode([]byte("fgh"))
	merge := merge(a, b)
	expected := [HashSize]byte{
		0x04, 0xea, 0x02, 0x9e, 0x23, 0xc2, 0xc2, 0x92,
		0x1b, 0x6d, 0x25, 0x48, 0x39, 0x35, 0x30, 0x55,
		0xd4, 0xee, 0xc6, 0x44, 0x34, 0xbc, 0x19, 0x9a,
		0xed, 0xe9, 0xe0, 0x1b, 0x9d, 0x7c, 0x87, 0x00,
	}
	if merge.hash != expected {
		fmt.Printf("merge: %x\n", merge.hash)
		fmt.Printf("expected: %x\n", expected)
		t.Fail()
	}
}
